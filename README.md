#Interview BankSealer
Project for interview in Banksealer

Required java 1.8+ and maven 3.3.9+

For generate jar go on root directory of project and run mvn install from command line, at the end of process jar will be found under target/demobanksealer-0.0.1-SNAPSHOT.jar

For start application run "java -jar demobanksealer-0.0.1-SNAPSHOT.jar" from command line

For start  jar in dev mode run "java -jar -Dspring.profiles.active=dev demobanksealer-0.0.1-SNAPSHOT.jar" from command line

See api from swagger ui when server is up on http://localhost:8080/swagger-ui.html

See in-memory database console on http://localhost:8080/h2-console and then set JDBC URL: jdbc:h2:mem:testdb

run mvn test for execute tests.

run mvn dependency:tree for see all dependencies of this project

Contact me for more at matteo.pipitone93@gmail.com