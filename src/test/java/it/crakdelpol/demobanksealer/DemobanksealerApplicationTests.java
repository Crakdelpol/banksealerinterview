package it.crakdelpol.demobanksealer;


import static org.assertj.core.api.Assertions.assertThat;

import it.crakdelpol.demobanksealer.controller.ConfigurationController;

import it.crakdelpol.demobanksealer.mapper.ConfigurationMapper;
import it.crakdelpol.demobanksealer.service.ConfigurationService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemobanksealerApplication.class)
public class DemobanksealerApplicationTests {

	@Autowired
	ConfigurationController configurationController;

	@Autowired
	ConfigurationMapper customConfigurationMapper;

	@Autowired
	ConfigurationService configurationService;

	@Test
	public void contextLoads() {

		assertThat(configurationController).isNotNull();
		assertThat(customConfigurationMapper).isNotNull();
		assertThat(configurationService).isNotNull();

	}

}
