package it.crakdelpol.demobanksealer.controller;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import it.crakdelpol.demobanksealer.DemobanksealerApplication;
import it.crakdelpol.demobanksealer.dto.ConfigurationDto;
import it.crakdelpol.demobanksealer.entity.Configuration;
import it.crakdelpol.demobanksealer.repository.ConfigurationRepository;
import it.crakdelpol.demobanksealer.service.ConfigurationService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemobanksealerApplication.class)
public class ConfigurationControllerTest {

    private MockMvc configurationControllerMockMvc;

    private static final String DEFAULT_ID = "FOO";
    private static final String DEFAULT_VALUE = "This is the value for configuration Foo";
    private static final String UPDATED_VALUE = "This is the value for configuration Bar";
    private static final String DEFAULT_NAME = "Configuration for Foo";
    private static final String UPDATED_NAME = "Configuration for Bar";

    @Autowired
    private ConfigurationService configurationService;

    @Autowired
    private ConfigurationRepository configurationRepository;

    private ConfigurationDto configurationDto;

    /** MediaType for JSON UTF8 */
    private static final MediaType APPLICATION_JSON_UTF8 = new MediaType(
            MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(), StandardCharsets.UTF_8);

    /**
     * Convert an object to JSON byte array.
     *
     * @param object
     *            the object to convert
     * @return the JSON byte array
     * @throws IOException
     */
    public static byte[] convertObjectToJsonBytes(Object object)
            throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);

        JavaTimeModule module = new JavaTimeModule();
        mapper.registerModule(module);

        return mapper.writeValueAsBytes(object);
    }

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.configurationControllerMockMvc = MockMvcBuilders
                .standaloneSetup(new ConfigurationController(configurationService))
                .build();
    }

    @Before
    public void initTest() {
        configurationDto = createConfiguratioDto();
    }

    public static ConfigurationDto createConfiguratioDto() {
        ConfigurationDto configurationDto = new ConfigurationDto();
        configurationDto.setId(DEFAULT_ID);
        configurationDto.setValue(DEFAULT_VALUE);
        configurationDto.setName(DEFAULT_NAME);
        return configurationDto;
    }

    @After
    public void tearDown() {
        configurationRepository.deleteAll();
    }

    @Test
    public void getAllConfiguration() throws Exception {

        int databaseSizeBeforeCreate = configurationRepository.findAll().size();

        int numberOfConfiguration = 4;
        for (int i = 0; i < numberOfConfiguration; i++){
            configurationControllerMockMvc.perform(post("/api/" + DEFAULT_ID + i)
                    .contentType(APPLICATION_JSON_UTF8)
                    .content(convertObjectToJsonBytes(configurationDto)))
                    .andExpect(status().isCreated());
        }

        List<Configuration> configurationList = configurationRepository.findAll();
        assertThat(configurationList).hasSize(databaseSizeBeforeCreate + numberOfConfiguration);

        String configurationDtoString = configurationControllerMockMvc.perform(get("/api"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(configurationDto.getId()+1)))
                .andReturn().getResponse().getContentAsString();

        Gson g = new Gson();
        List<LinkedTreeMap> configurationDtoList = g.fromJson(configurationDtoString, ArrayList.class);
        assertThat(configurationDtoList).hasSize(databaseSizeBeforeCreate + numberOfConfiguration);

        for (int i = 0; i < numberOfConfiguration; i++){
            assertThat(configurationDtoList.get(i).get("id")).isEqualTo(DEFAULT_ID + i);
        }
    }

    @Test
    public void getByIdentifier() throws Exception {

        configurationControllerMockMvc.perform(post("/api/" + DEFAULT_ID)
                .contentType(APPLICATION_JSON_UTF8)
                .content(convertObjectToJsonBytes(configurationDto)))
                .andExpect(status().isCreated());


        String result = configurationControllerMockMvc.perform(get("/api/"+ DEFAULT_ID))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)).andReturn().getResponse().getContentAsString();
        Gson gson = new Gson();
        ConfigurationDto resultDto = gson.fromJson(result, ConfigurationDto.class);

        assertThat(resultDto.getId()).isEqualTo(DEFAULT_ID);
        assertThat(resultDto.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(resultDto.getValue()).isEqualTo(DEFAULT_VALUE);

    }

    @Test
    public void createConfiguration() throws Exception {

        int databaseSizeBeforeCreate = configurationRepository.findAll().size();

        configurationControllerMockMvc.perform(post("/api/" + DEFAULT_ID)
                .contentType(APPLICATION_JSON_UTF8)
                .content(convertObjectToJsonBytes(configurationDto)))
                .andExpect(status().isCreated());

        // Validate the Bookmaker in the database
        List<Configuration> configurationList = configurationRepository.findAll();
        assertThat(configurationList).hasSize(databaseSizeBeforeCreate + 1);
        Configuration configuration = configurationList.get(0);

        assertThat(configuration.getIdentifier()).isEqualTo(DEFAULT_ID);
        assertThat(configuration.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(configuration.getValue()).isEqualTo(DEFAULT_VALUE);

    }

    @Test
    public void updateConfiguration() throws Exception {

        configurationControllerMockMvc.perform(post("/api/" + DEFAULT_ID)
                .contentType(APPLICATION_JSON_UTF8)
                .content(convertObjectToJsonBytes(configurationDto)))
                .andExpect(status().isCreated());

        ConfigurationDto configurationDtoUpdated = new ConfigurationDto();
        configurationDtoUpdated.setName(UPDATED_NAME);
        configurationDtoUpdated.setValue(UPDATED_VALUE);

        configurationControllerMockMvc.perform(put("/api/" + DEFAULT_ID)
                .contentType(APPLICATION_JSON_UTF8)
                .content(convertObjectToJsonBytes(configurationDtoUpdated)))
                .andExpect(status().isOk());


        String result = configurationControllerMockMvc.perform(get("/api/"+ DEFAULT_ID))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)).andReturn().getResponse().getContentAsString();

        Gson gson = new Gson();
        ConfigurationDto resultDto = gson.fromJson(result, ConfigurationDto.class);

        assertThat(resultDto.getId()).isEqualTo(DEFAULT_ID);
        assertThat(resultDto.getName()).isEqualTo(UPDATED_NAME);
        assertThat(resultDto.getValue()).isEqualTo(UPDATED_VALUE);

    }
}