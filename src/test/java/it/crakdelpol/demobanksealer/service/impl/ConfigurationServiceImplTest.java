package it.crakdelpol.demobanksealer.service.impl;

import it.crakdelpol.demobanksealer.DemobanksealerApplication;
import it.crakdelpol.demobanksealer.controller.ConfigurationControllerTest;
import it.crakdelpol.demobanksealer.dto.ConfigurationDto;
import it.crakdelpol.demobanksealer.entity.Configuration;
import it.crakdelpol.demobanksealer.mapper.ConfigurationMapper;
import it.crakdelpol.demobanksealer.repository.ConfigurationRepository;
import it.crakdelpol.demobanksealer.service.ConfigurationService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemobanksealerApplication.class)
public class ConfigurationServiceImplTest {

    @Autowired
    ConfigurationService configurationService;

    @Autowired
    ConfigurationRepository configurationRepository;

    @After
    public void tearDown(){
        configurationRepository.deleteAll();
    }

    @Test
    public void save() {

        ConfigurationDto configurationDto = ConfigurationControllerTest.createConfiguratioDto();
        configurationService.save(configurationDto);

        Configuration configuration = configurationRepository.findByIdentifier(configurationDto.getId()).orElse(new Configuration());

        ConfigurationDto configurationDtoFinded = ConfigurationMapper.MAPPER.toConfigurationDto(configuration);

        assertEquals(configurationDtoFinded.getName(), (configurationDto.getName()));
        assertEquals(configurationDtoFinded.getValue(), (configurationDto.getValue()));
    }

    @Test
    public void findOne() {

        Configuration configuration = ConfigurationMapper.MAPPER.toConfiguration(ConfigurationControllerTest.createConfiguratioDto());
        configurationRepository.save(configuration);

        ConfigurationDto configurationDtoSaved = ConfigurationMapper.MAPPER.toConfigurationDto(configuration);

        ConfigurationDto configurationDtoFinded = configurationService.findOne(configuration.getIdentifier()).orElse(new ConfigurationDto());

        assertEquals(configurationDtoSaved.getName(), (configurationDtoFinded.getName()));
        assertEquals(configurationDtoSaved.getValue(), (configurationDtoFinded.getValue()));
    }

    @Test
    public void findAll() {
        ConfigurationDto configurationDto;

        int size = 3;
        for(int i = 0; i < size; i++){

            configurationDto  = ConfigurationControllerTest.createConfiguratioDto();
            configurationDto.setId(configurationDto.getId() + i);
            Configuration configuration = ConfigurationMapper.MAPPER.toConfiguration(configurationDto);
            configurationRepository.save(configuration);
        }


        List<ConfigurationDto> configurationDtos = configurationService.findAll();
        assertEquals(configurationDtos.size(), size);

        configurationDto  = ConfigurationControllerTest.createConfiguratioDto();

        for(int i = 0; i < configurationDtos.size(); i++){

            assertEquals(configurationDtos.get(i).getId(), configurationDto.getId()+i);
            assertEquals(configurationDtos.get(i).getName(), configurationDto.getName());
            assertEquals(configurationDtos.get(i).getValue(), configurationDto.getValue());

        }
    }

    @Test
    public void delete() {

        Configuration configuration = ConfigurationMapper.MAPPER.toConfiguration(ConfigurationControllerTest.createConfiguratioDto());
        configurationRepository.save(configuration);

        int databaseSize = configurationRepository.findAll().size();
        assertEquals(databaseSize, 1);
        configurationService.delete(configuration.getIdentifier());

        databaseSize = configurationRepository.findAll().size();
        assertEquals(databaseSize, 0);

    }
}