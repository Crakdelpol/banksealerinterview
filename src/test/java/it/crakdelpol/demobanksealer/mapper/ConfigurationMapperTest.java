package it.crakdelpol.demobanksealer.mapper;

import it.crakdelpol.demobanksealer.DemobanksealerApplication;
import it.crakdelpol.demobanksealer.dto.ConfigurationDto;
import it.crakdelpol.demobanksealer.entity.Configuration;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@SpringBootTest(classes = DemobanksealerApplication.class)
public class ConfigurationMapperTest {

    @Test
    public void fromConfigurationToConfigurationDto(){

        Configuration configuration1 = new Configuration();
        configuration1.setIdentifier("foo");
        configuration1.setName("Configuration for Foo");
        configuration1.setValue("This is the value for configuration Foo");

        ConfigurationDto configurationDto = ConfigurationMapper.MAPPER.toConfigurationDto(configuration1);

        checkEquals(configuration1, configurationDto);

        Configuration configuration2 = new Configuration();
        configuration2.setIdentifier("bar");
        configuration2.setName("Configuration for Bar");
        configuration2.setValue("This is the value for configuration Bar");

        List<Configuration> configurations = new ArrayList<>();
        configurations.add(configuration1);
        configurations.add(configuration2);

        List<ConfigurationDto> configurationDtos = ConfigurationMapper.MAPPER.toConfigurationDtos(configurations);
        for (int i = 0; i < configurationDtos.size(); i++){
            checkEquals(configurations.get(i), configurationDtos.get(i));
        }

    }

    private void checkEquals(Configuration configuration, ConfigurationDto configurationDto){

        assertEquals(configurationDto.getId(), configuration.getIdentifier());
        assertEquals(configurationDto.getName(), configuration.getName());
        assertEquals(configurationDto.getValue(), configuration.getValue());

    }
    @Test
    public void formConfigurationDtoToConfiguration(){

        ConfigurationDto configurationDto1 = new ConfigurationDto();
        configurationDto1.setId("foo");
        configurationDto1.setName("Configuration for Foo");
        configurationDto1.setValue("This is the value for configuration Foo");

        Configuration configuration = ConfigurationMapper.MAPPER.toConfiguration(configurationDto1);

        checkEquals(configuration, configurationDto1);

        ConfigurationDto configurationDto2 = new ConfigurationDto();
        configurationDto2.setId("bar");
        configurationDto2.setName("Configuration for Bar");
        configurationDto2.setValue("This is the value for configuration Bar");

        List<ConfigurationDto> configurationDtos = new ArrayList<>();
        configurationDtos.add(configurationDto1);
        configurationDtos.add(configurationDto2);

        List<Configuration> configurations = ConfigurationMapper.MAPPER.toConfigurations(configurationDtos);
        for (int i = 0; i < configurations.size(); i++){
            checkEquals(configurations.get(i), configurationDtos.get(i));
        }

    }

}