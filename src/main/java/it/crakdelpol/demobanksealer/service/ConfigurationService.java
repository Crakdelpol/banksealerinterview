package it.crakdelpol.demobanksealer.service;

import it.crakdelpol.demobanksealer.dto.ConfigurationDto;

import java.util.List;
import java.util.Optional;


public interface ConfigurationService {

    ConfigurationDto save(ConfigurationDto configuration);

    Optional<ConfigurationDto> findOne(String identifier);

    List<ConfigurationDto> findAll();

    void delete(String identifier);

}
