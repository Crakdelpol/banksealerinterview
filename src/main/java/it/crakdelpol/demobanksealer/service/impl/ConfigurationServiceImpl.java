package it.crakdelpol.demobanksealer.service.impl;

import it.crakdelpol.demobanksealer.dto.ConfigurationDto;
import it.crakdelpol.demobanksealer.entity.Configuration;

import it.crakdelpol.demobanksealer.mapper.ConfigurationMapper;
import it.crakdelpol.demobanksealer.repository.ConfigurationRepository;
import it.crakdelpol.demobanksealer.service.ConfigurationService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional()
public class ConfigurationServiceImpl implements ConfigurationService {

    final ConfigurationRepository configurationRepository;
    final ConfigurationMapper configurationMapper;

    public ConfigurationServiceImpl(ConfigurationRepository configurationRepository, ConfigurationMapper configurationMapper) {
        this.configurationRepository = configurationRepository;
        this.configurationMapper = configurationMapper;
    }


    @Override
    @Transactional()
    public ConfigurationDto save(ConfigurationDto configurationDto) {

        Configuration configuration = configurationMapper.toConfiguration(configurationDto);
        Optional<Configuration> configurationOptional = configurationRepository.findByIdentifier(configuration.getIdentifier());

        //override all field excepted for id
        configurationOptional.ifPresent(configuration1 -> {
            configuration1.setIdentifier(configuration.getIdentifier());
            configuration1.setName(configuration.getName());
            configuration1.setValue(configuration.getValue());
        });

        Configuration configurationSaved =
                configurationRepository.save(configurationOptional.orElse(configuration));

        return configurationMapper.toConfigurationDto(configurationSaved);

    }

    @Override
    @Transactional()
    public Optional<ConfigurationDto> findOne(String identifier) {
        return configurationRepository.findByIdentifier(identifier).map(configurationMapper::toConfigurationDto);
    }

    @Override
    @Transactional()
    public List<ConfigurationDto> findAll() {
        return configurationRepository
                .findAll()
                .stream()
                .map(configurationMapper::toConfigurationDto).collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional()
    public void delete(String identifier) {
        configurationRepository.deleteByIdentifier(identifier);
    }


}
