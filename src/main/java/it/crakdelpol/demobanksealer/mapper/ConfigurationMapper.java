package it.crakdelpol.demobanksealer.mapper;

import it.crakdelpol.demobanksealer.dto.ConfigurationDto;
import it.crakdelpol.demobanksealer.entity.Configuration;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface ConfigurationMapper {

    ConfigurationMapper MAPPER = Mappers.getMapper( ConfigurationMapper.class );

    @Mapping(source = "id", target = "identifier")
    @Mapping(target = "id", ignore = true)
    Configuration toConfiguration(ConfigurationDto configurationDto);

    @IterableMapping(qualifiedByName = "toConfiguration")
    List<Configuration> toConfigurations(List<ConfigurationDto> configurationDto);

    @InheritInverseConfiguration
    ConfigurationDto toConfigurationDto(Configuration configuration);

    @IterableMapping(qualifiedByName = "toConfigurationDto")
    List<ConfigurationDto> toConfigurationDtos(List<Configuration> configuration);

}
