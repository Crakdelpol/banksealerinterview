package it.crakdelpol.demobanksealer.config;

import it.crakdelpol.demobanksealer.aop.Logging;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;

@Configuration
public class LoggingAspectConfiguration {

    @Bean
    @Profile(Constant.SPRING_PROFILE_DEVELOPMENT)
    public Logging logging(Environment env) {
        return new Logging(env);
    }
}
