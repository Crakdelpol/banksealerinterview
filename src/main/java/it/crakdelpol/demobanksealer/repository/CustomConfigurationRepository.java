package it.crakdelpol.demobanksealer.repository;

import it.crakdelpol.demobanksealer.entity.Configuration;
import java.util.Optional;

public interface CustomConfigurationRepository {

    Optional<Configuration> findByIdentifier(String identifier);

    void deleteByIdentifier(String identifier);

}
