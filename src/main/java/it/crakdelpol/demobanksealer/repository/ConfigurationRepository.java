package it.crakdelpol.demobanksealer.repository;

import it.crakdelpol.demobanksealer.entity.Configuration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConfigurationRepository extends JpaRepository<Configuration, Long>, CustomConfigurationRepository {
}
