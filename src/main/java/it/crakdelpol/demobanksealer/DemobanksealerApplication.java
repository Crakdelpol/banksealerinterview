package it.crakdelpol.demobanksealer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages = "it.crakdelpol.demobanksealer.repository")
@EnableAspectJAutoProxy
public class DemobanksealerApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemobanksealerApplication.class, args);
	}

}
