package it.crakdelpol.demobanksealer.entity;

import javax.persistence.*;

@Entity
@Table(name="CONFIGURATION")
public class Configuration {

    @Id
    @GeneratedValue
    private Long id;

    private String identifier;
    private String name;
    private String value;

    public Configuration() { }

    @Column(name = "IDENTIFIER", unique = true)
    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    @Column(name = "NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "VALUE")
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
