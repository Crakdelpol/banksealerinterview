package it.crakdelpol.demobanksealer.dto;

import io.swagger.annotations.ApiModelProperty;

public class ConfigurationDto {

    @ApiModelProperty(notes = "The configuration's ID")
    private String id;
    @ApiModelProperty(notes = "The configuration's Name")
    private String name;
    @ApiModelProperty(notes = "The configuration's Value")
    private String value;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "ConfigurationDto{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
