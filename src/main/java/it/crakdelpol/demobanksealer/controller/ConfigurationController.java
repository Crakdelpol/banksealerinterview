package it.crakdelpol.demobanksealer.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import it.crakdelpol.demobanksealer.dto.ConfigurationDto;
import it.crakdelpol.demobanksealer.service.ConfigurationService;
import it.crakdelpol.demobanksealer.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@RestController()
@RequestMapping("/api")
@Api(value="Api description")
public class ConfigurationController {

    private final
    ConfigurationService configurationService;

    private Logger log = LoggerFactory.getLogger(ConfigurationController.class);

    public ConfigurationController(ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    @GetMapping()
    @ApiOperation(value = "View a list of configurations",response = List.class)
    public List<ConfigurationDto> getAllConfiguration(){
        return configurationService.findAll();
    }

    @GetMapping("/{identifier}")
    @ApiOperation(value = "View a configuration with identifier",response = ConfigurationDto.class)
    public ResponseEntity<ConfigurationDto> getByIdentifier(@PathVariable String identifier){

        Optional<ConfigurationDto> configuration =  configurationService.findOne(identifier);
        return Util.wrapOrNotFound(configuration);

    }

    @PostMapping("/{identifier}")
    @ApiOperation(value = "Add a configuration")
    public ResponseEntity<ConfigurationDto> createConfiguration(@PathVariable String identifier, @RequestBody ConfigurationDto configurationDto) throws URISyntaxException {

        configurationDto.setId(identifier);

        log.debug("REST request to save configuration : {}", configurationDto);

        if(configurationService.findOne(identifier).isPresent()){
            throw new ResponseStatusException(
                    HttpStatus.CONFLICT, String.format("Configuration with id %s exists!", identifier));
        }

        ConfigurationDto resultDto = configurationService.save(configurationDto);

        return ResponseEntity.created(new URI("/api" + resultDto.getId()))
                .body(resultDto);

    }

    @PutMapping("/{identifier}")
    @ApiOperation(value = "Update a configuration")
    public ResponseEntity<ConfigurationDto> updateConfiguration(@PathVariable String identifier, @RequestBody ConfigurationDto configurationDto){
        log.debug("REST request to update Configuration : {}", configurationDto);

        configurationDto.setId(identifier);
        ConfigurationDto result = configurationService.save(configurationDto);

        return ResponseEntity.ok().body(result);
    }

    @DeleteMapping("/{identifier}")
    @ApiOperation(value = "Delete a configuration")
    public ResponseEntity<Void> updateConfiguration(@PathVariable String identifier){
        log.debug("REST request to delete Configuration : {}", identifier);

        configurationService.delete(identifier);
        return ResponseEntity.ok().build();
    }


}
